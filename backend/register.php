<?php

require 'db_config.php';
session_start();

// Set session variables to be used on profile.php page
$_SESSION['email']      = $_POST['email'];
$_SESSION['first_name'] = $_POST['first_name'];
$_SESSION['last_name']  = $_POST['last_name'];
$_SESSION['username']   = $_POST['username'];
$_SESSION['avatar']     = $_POST['avatar'];
$_SESSION['bio']        = $_POST['bio'];

// Escape all $_POST variables to protect against SQL injections
$first_name = $mysqli->escape_string($_POST['first_name']);
$last_name  = $mysqli->escape_string($_POST['last_name']);
$email      = $mysqli->escape_string($_POST['email']);
$username   = $mysqli->escape_string($_POST['username']);
$password   = $mysqli->escape_string(password_hash($_POST['password'], PASSWORD_BCRYPT));
$hash       = $mysqli->escape_string(md5(rand(0, 1000)));

// Check if user with that email already exists
$result = $mysqli->query("SELECT * FROM users WHERE email='$email'") or die($mysqli->error);

// We know user email exists if the rows returned are more than 0
if ($result->num_rows > 0) {
    
    // error will be a modal
    $_SESSION['message'] = 'User with this email already exists!';
    
} else { // Email doesn't already exist in a database, proceed...
    
    // active is 0 by DEFAULT (no need to include it here)
    $sql = "INSERT INTO users (first_name, last_name, email, username, password, hash) " . "VALUES ('$first_name','$last_name','$email', '$username' ,'$password', '$hash')";
    
    // Add user to the database
    if ($mysqli->query($sql)) {
        
        $_SESSION['active']    = 0; //0 until user activates their account with verify.php
        $_SESSION['logged_in'] = true; // So we know the user has logged in
        $_SESSION['message']   = "Confirmation link has been sent to $email, please verify
                 your account by clicking on the link in the message!";
    }
    
    else {
        // error will be a modal
        $_SESSION['message'] = 'Registration failed!';
    }
    
}