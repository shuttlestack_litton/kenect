<!doctype html>
<html lang="en" class="">
<?php $title = 'Get Connected, Get Kenect.'; require 'templates/source.php'; ?>
<?php session_start(); require 'backend/db_config.php';  ?>

<body>

    <?php
    
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        if (isset($_POST['login'])) { // login
            require 'backend/login.php';
        } 
        elseif (isset($_POST['register'])) { // registration
            require 'backend/register.php';
        }
    }
    
    ?>

        <section class="space-xs text-center bg-gradient text-light">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <i class="mr-1 icon-cake"></i>
                        <span class="mr-2">Receive a $100 bonus by becoming a Kenect DJ this month.</span>
                        <a href="#" class="text-white" data-toggle="modal" data-target="#bonusModal">Learn More &rsaquo;</a>
                    </div>
                    <!--end of col-->
                </div>
                <!--end of row-->
            </div>
            <!--end of container-->
        </section>
        <!--end of section-->

        <?php include 'templates/header.php'; ?>

        <div class="main-container">

            <section class="bg-dark space-lg">
                <div class="container">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-12 col-md-6 col-lg-5 section-intro">
                            <h1 class="display-3">Get Connected, Get Kenect.</h1>
                            <span class="lead">
                Kenect gives partygoers the ultimate nightlife experience, providing direct connection to the DJ, without barriers.
              </span>
                        </div>
                        <!--end of col-->
                        <div class="col-12 col-md-6 col-lg-5">
                            <div class="card">
                                <div class="card-body">
                                    <form action="" method="POST">
                                        <div class="form-group">
                                            <label for="exampleInputUsername">First Name</label>
                                            <input type="text" class="form-control form-control-lg" name="first_name" id="exampleInputUsername" placeholder="What's Your First Name?" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputUsername">Last Name</label>
                                            <input type="text" class="form-control form-control-lg" name="last_name" id="exampleInputUsername" placeholder="What's Your Last Name?" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputUsername">Desired Username</label>
                                            <input type="text" class="form-control form-control-lg" name="username" id="exampleInputUsername" placeholder="What's Your Stage Name?" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail">Email address</label>
                                            <input type="email" class="form-control form-control-lg" id="exampleInputEmail" name="email" placeholder="What's Your Email Address?" required>
                                            <small id="emailHelp" class="form-text">We'll never share your email with anyone else.</small>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword">Password</label>
                                            <input type="password" class="form-control form-control-lg" name="password" id="exampleInputPassword" placeholder="This Is Super Secret..." required>
                                        </div>
                                        <div class="mb-3">
                                            <small>By signig up you agree to our <a href="#">Terms &amp; Conditions</a></small>
                                        </div>
                                        <button type="submit" name="register" id="register" class="btn btn-lg btn-primary bg-gradient btn-block">Create Account</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <!--end of section-->

            <section>
                <div class="container">
                    <div class="row justify-content-center text-center section-intro">
                        <div class="col-12 col-md-9 col-lg-8">
                            <span class="title-decorative">It Feels Good To Be a DJ</span>
                            <h2 class="display-4">What Is Kenect?</h2>
                            <span class="lead">Kenect is a platform that enables users to request their favorite songs from the DJ performing at the venue their connected to.</span>
                            <span class="lead">This gives DJ's a new side hustle to make extra income from.</span>

                        </div>
                        <!--end of col-->
                    </div>

                </div>
                <!--end of container-->
            </section>
            <!--end of section-->

            <section class="bg-white fullwidth-split">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 col-sm-5 col-lg-6 order-sm-2 fullwidth-split-image">
                            <img alt="Image" src="static/img/section-1.jpg" class="bg-image" />
                        </div>
                        <!--end of col-->
                        <div class="col-12 col-sm-7 col-lg-6 order-sm-1 fullwidth-split-text">
                            <div class="col-12 col-sm-8">
                                <h3 class="h1">The Next Big Thing Is Here.</h3>
                                <span class="lead">Easy to use platform for DJs to make extra income.</span>
                                <hr class="short" />
                                <ul class="feature-list feature-list-sm">
                                    <li>
                                        <div class="media">
                                            <div class="media-body">
                                                <h6>DJ</h6>
                                                <p>
                                                    Accept or deny song requests with just a simple click of a button. Receive income for songs you accept.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <div class="media-body">
                                                <h6>User</h6>
                                                <p>
                                                    Request songs that you'd like to bump at a club, wedding, or after-party. Not accepted? Don't worry, you won't be charged.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end of col-->
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <!--end of section-->

            <section class="bg-white fullwidth-split">
                <div class="container-fluid">
                    <div class="row no-gutters">
                        <div class="col-12 col-sm-5 col-lg-6 fullwidth-split-image">
                            <img alt="Image" src="static/img/section-2.jpg" class="bg-image" />
                        </div>
                        <!--end of col-->
                        <div class="col-12 col-sm-7 col-lg-6 fullwidth-split-text">
                            <div class="col-12 col-sm-8">
                                <h3 class="h1">Get Closer To Your Audience.</h3>
                                <span class="lead">Learn about your audience's music taste.</span>
                                <hr class="short" />
                                <ul class="feature-list feature-list-sm">
                                    <li>
                                        <div class="media">
                                            <div class="media-body">
                                                <p>
                                                    You'll learn about your audience's music taste by the songs they're requesting.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="media">
                                            <div class="media-body">
                                                <p>
                                                    Your audience wants their favorite songs played. Get paid for playing what they want to hear.
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end of col-->
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <!--end of section-->


            <section>
                <div class="container">
                    <div class="row justify-content-center text-center section-intro">
                        <div class="col-12 col-md-9 col-lg-8">
                            <span class="title-decorative">Fair Pricing</span>
                            <h2 class="display-4">We Put You First</h2>
                            <span class="lead">Kenect has a fair 70/30 split with our DJs. Bonuses will be given from time to time to show our appreciation.</span>

                        </div>
                        <!--end of col-->
                    </div>
                    <!--end of row-->
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-6 row no-gutters card-group">
                            <div class="card pricing card-lg bg-gradient col-lg-6">
                                <div class="card-body">
                                    <h5>YOU</h5>
                                    <span class="display-3">70%</span>
                                    <span class="text-small">TAKE HOME</span>
                                    <ul class="list-unstyled">
                                        <br>
                                        <p>We believe that the fees should be fair. You take home 70% for your hard work and dedication.</p>
                                    </ul>
                                </div>
                            </div>
                            <!--end card-->
                            <div class="card pricing card-lg col-lg-6">
                                <div class="card-body">
                                    <h5>US</h5>
                                    <span class="display-3">30%</span>
                                    <span class="text-small">SERVICE FEE</span>
                                    <ul class="list-unstyled">
                                        <br>
                                        <p>We believe that the fees should be fair. We charge 30% in order to continue our service.</p>
                                    </ul>
                                </div>
                            </div>
                            <!--end card-->
                        </div>
                        <!--end of col-->
                    </div>
                    <!--end of row-->

            </section>
            <!--end of section-->

            <section>
                <div class="container">
                    <div class="row justify-content-center text-center section-intro">
                        <div class="col-12 col-md-9 col-lg-8">
                            <h2 class="display-4">What Do You Think?</h2>
                            <span class="lead">Get started now and receive a $100 bonus during your first performance using Kenect.</span>
                            <button type="submit" onclick="createAccountFunction()" id="createAccount" class="btn btn-lg btn-primary bg-gradient btn-block">Create An Account</button>
                        </div>
                        <!--end of col-->
                    </div>

                </div>
                <!--end of container-->
            </section>
            <!--end of section-->

            <?php include 'templates/footer.php'; ?>

            </div>

            <!-- login system... -->
            <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal modal-center-viewport" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header-borderless justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times"></i>
                            </span>
                          </button>
                        </div>
                        <div class="modal-body d-flex justify-content-center">
                            <div class="w-75">
                                <span class="h1">Login</span>
                                <p>Welcome back to Kenect, login to enter your account.</p>
                                <form action="" method="POST">
                                    <div class="form-group">
                                        <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" name="email" placeholder="What's Your Email?" required>
                                        <br />
                                        <input type="password" class="form-control form-control-lg" id="exampleInputEmail1" name="password" placeholder="What's Your Password?" required>
                                    </div>
                                    <div class="form-group">
                                        <a href="#">Forgot Password?</a>
                                    </div>
                                    <button type="submit" name="login" id="login" class="btn btn-lg btn-primary bg-gradient btn-block">Login</button>
                                </form>
                            </div>
                        </div>
                        <div class="modal-footer modal-footer-borderless justify-content-center">

                        </div>
                    </div>
                </div>
            </div>


            <!-- bonus learn more... -->
            <div class="modal fade" id="bonusModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-center-viewport" role="document">
                    <div class="modal-content">
                        <div class="modal-header modal-header-borderless justify-content-end">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times"></i>
                            </span>
                          </button>
                        </div>
                        <div class="modal-body d-flex justify-content-center">
                            <div class="w-75">
                                <h1>$100 Bonus for Signing Up</h1>
                                <hr />
                                <small><b>Introduction</b></small>
                                <p>Hi, thank you for taking interest in being a DJ for Kenect. We are offering a $100 bonus for DJ's that sign up in the month(s) of February-April. We are giving this promotion to DJ's in order to show our appreciation for
                                    using our platform.</p>
                                <small><b>How It Works</b></small>
                                <p>After signing up, you'll have a pending credit of $100 in your account. There are two ways you can access this credit. You can use it to request songs from other DJ's on our mobile app or you can receive it as a bonus to
                                    withdraw after your first live performance using Kenect.</p>
                                <h4>We want to thank you for being apart of Kenect's future.</h4>
                            </div>
                        </div>
                        <div class="modal-footer modal-footer-borderless justify-content-center">

                        </div>
                    </div>
                </div>
            </div>

            <?php require 'templates/scripts.php'; ?>
            <script type="text/javascript">
                function createAccountFunction() {
                    document.body.scrollTop = 200; // For Safari
                    document.documentElement.scrollTop = 200; // For Chrome, Firefox, IE and Opera
                }
            </script>

</body>

</html>
