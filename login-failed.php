<!doctype html>
<html lang="en" class="">
    <?php $title = 'Login'; require 'templates/source.php'; ?>
    <body>

    <div class="main-container">
      <section class="fullwidth-split">
        <div class="container-fluid">
          <div class="row no-gutters height-100 justify-content-center">
            <div class="col-12 col-lg-6 fullwidth-split-image bg-dark d-flex justify-content-center align-items-center">
              <img alt="Image" src="static/img/headset.jpg" class="bg-image position-absolute opacity-30" />
              <div class="col-12 col-sm-8 col-lg-9 text-center pt-5 pb-5">
                <img alt="Image" src="static/img/logo.png" class="mb-4 logo-lg" height="60" />
                <span class="h3 mb-5">Generate Income by Performing with Song Requests.</span>

                <div class="card text-left">
                  <div class="card-body">
                    <div class="media">
                      <div class="media-body">
                        <small>Daily Tip</small>
                        <p class="mb-1">
                          Add the song to your queue to play it automatically without stopping your current song.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <!--end of col-->
            </div>
            <!--end of col-->
            <div class="col-12 col-sm-8 col-lg-6 fullwidth-split-text">
              <div class="col-12 col-lg-8 align-self-center">
                <div class="text-center mb-5">
                  <h1 class="h2 mb-2">Login</h1>
                  <span>Sign in to your account to continue.</span>
                </div>
                <form class="mb-4">
                  <div class="form-group">
                    <label for="signup-email">Email Address</label>
                    <input class="form-control form-control-lg" type="email" name="email" id="signup-email" placeholder="Email Address" />
                  </div>
                  <div class="form-group">
                    <label for="signup-password">Password</label>
                    <input class="form-control form-control-lg" type="password" name="password" id="signup-password" placeholder="Password" />
                  </div>
                  <div>
                  </div>
                  <div class="text-center mt-4">
                    <button type="submit" class="btn btn-lg btn-block btn-primary">Login</button>
                  </div>
                </form>
                <div class="text-center">
                  <span class="text-small">Already have an account? <a href="#">Log in here</a>
                  </span>
                </div>
              </div>
              <!--end of col-->
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>
      <!--end of section-->
    </div>

    <?php require 'templates/scripts.php'; ?>

  </body>

</html>

