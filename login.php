<!doctype html>
<html lang="en" class="">
    <?php $title = 'Get Connected, Get Kenect.'; require 'templates/source.php'; ?>
  <body>

    <div class="main-container" style="background: #1d1d1d">
      <section class="space-sm">
        <div class="container align-self-start">
          <div class="row mb-5">
            <div class="col text-center">
              <a href="#">
                <img alt="Image" src="static/img/logo.png" height="60" />
              </a>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
          <div class="row justify-content-center">
            <div class="col-12 col-md-8 col-lg-7">
              <div class="card card-lg text-center" style="background: #1d1d1d;">
                <div class="card-body">
                  <div class="mb-5">
                    <h1 class="h2 mb-2" style="color: #fff;">Hello again</h1>
                    <span style="color: #fff;">Sign in to your account to continue</span>
                  </div>
                  <div class="row no-gutters justify-content-center">
                    <form class="text-left col-lg-8">
                      <div class="form-group">
                        <input class="form-control form-control-lg" type="email" name="email" id="login-email" placeholder="Email Address" />
                      </div>
                      <div class="form-group">
                        <input class="form-control form-control-lg" type="password" name="password" id="login-password" placeholder="Enter a password" />
                        <small style="color: #fff;">Forgot password? <a href="#" style="color: #fff;">Reset here</a>
                        </small>
                      </div>
                      <div class="text-center mt-3">
                        <button type="submit" class="btn btn-lg btn-primary">Log in</button>
                      </div>
                    </form>
                  </div>
                  <!--end of row-->
                </div>
              </div>
              <div class="text-center">
                <span class="text-small">Don't have an account yet? <a href="#">Create one</a>
                </span>
              </div>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>
      <!--end of section-->
    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/smartwizard@4.3.1/dist/js/jquery.smartWizard.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/flickity/2.1.2/flickity.pkgd.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/scrollmonitor@1.2.4/scrollMonitor.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/12.1.5/js/smooth-scroll.polyfills.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/prism/1.15.0/prism.min.js"></script>
    <script type="text/javascript" src="https://unpkg.com/zoom-vanilla.js@2.0.6/dist/zoom-vanilla.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/theme.js"></script>

  </body>

</html>

