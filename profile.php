<?php 
require 'backend/db_config.php';
session_start();
 
$query = "SELECT * FROM users";

$_SESSION['logged_in'] = true;
 
if ($result = $mysqli->query($query)) {
 
    while ($row = $result->fetch_assoc()) {
        $email = $row["email"];
        $first_name = $row['first_name'];
        $last_name = $row['last_name'];
        $username = $row['username'];
        $avatar = $row['avatar'];
        $bio = $row['bio'];
    }
 
/*freeresultset*/
$result->free();
}
?>
<!doctype html>
<html lang="en" class="">
    <?php $title = 'Profile'; require 'templates/source.php'; ?>
    <body>
    <?php include 'templates/header-in.php'; ?>
    <div class="main-container">
      <section class="bg-white">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-12 col-md-8">
              <div class="media">
                <img alt="Image" src="<?php print $avatar; ?>" class="mr-md-5 avatar avatar-xlg" />
                <div class="media-body">
                  <br><div class="mb-3">
                    <h1 class="h2 mb-2"><?php print $username; ?></h1>
                  </div>
                  <p>
                   <?php print $bio; ?>
                  </p>
                  </div>
                </div>
              </div>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>
      <!--end of section-->
      <section class="flush-with-above space-0">
        <div class="bg-white">
          <div class="container">
            <div class="row">
              <div class="col">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="projects-tab" data-toggle="tab" href="#projects" role="tab" aria-controls="projects" aria-selected="true">Activity</a>
                  </li>
                </ul>
              </div>
              <!--end of col-->
            </div>
            <!--end of row-->
          </div>
          <!--end of container-->
        </div>
      </section>
      <section class="flush-with-above">
        <div class="tab-content">
          <div class="tab-pane fade show active" id="projects" role="tabpanel" aria-labelledby="projects-tab">
            <div class="container">
              <ul class="row feature-list feature-list-sm">

                <li class="col-12 col-md-6 col-lg-4">
                  <div class="card" style="border-radius: 30px;">
                    <a href="#">
                      <img class="card-img-top" src="https://cdn.dribbble.com/users/6360/screenshots/3586886/trophy.png" alt="Card image cap">
                    </a>
                    <div class="card-body">
                      <a href="#">
                        <h4 class="card-title">Joined Kenect!</h4>
                        <p class="card-text text-body"><?php print $first_name; ?> is now a DJ on Kenect!</p>
                      </a>
                    </div>
                  </div>
                </li>
                <!--end of col-->

              
            </div>
            <!--end of container-->
          </div>
        </div>
      </section>
      <!--end of section-->
               

               
               

              </div>
              <!--end of row-->
            </div>
            <!--end of container-->
          </div>
        </div>
      </section>
      
      <?php require 'templates/scripts.php'; ?>
      </body>
      </html>