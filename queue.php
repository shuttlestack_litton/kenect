<?php 
require 'backend/db_config.php';
session_start();
 
$query = "SELECT * FROM users";
 
if ($result = $mysqli->query($query)) {
 
    while ($row = $result->fetch_assoc()) {
        $email = $row["email"];
        $first_name = $row['first_name'];
        $last_name = $row['last_name'];
        $username = $row['username'];
        $avatar = $row['avatar'];
    }
 
/*freeresultset*/
$result->free();
}
?>
<!doctype html>
<html lang="en" class="">
<?php $title = 'Queue'; require 'templates/source.php'; ?>

<body>

    <?php include 'templates/header-in.php'; ?>

    <section class="height-70">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6 col-lg-5">
                    <div class="card card-lg text-center">
                        <div class="card-body">
                            <i class="fa fa-play-circle display-4 opacity-20"></i>
                            <h1 class="h5">You Have No Songs In Your Queue.</h1>
                            <p>
                               When a user requests a song, you'll be able to add it to your queue so you can play one after another.
                            </p>
                            <div class="form-group">
                                <button class="btn btn-lg btn-primary" onclick="location.href='home'"  type="submit">View Requests </button>
                             </div>
                             </form>
                        </div>
                    </div>
                </div>
                <!--end of col-->
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    
     <section class="space-xs text-center bg-gradient text-light">
        <div class="container">
          <div class="row">
            <div class="col">
              <i class="mr-1 icon-cake"></i>
              <span class="mr-2">Please answer a few questions in our survey.</span>
              <a href="survey" class="text-white">Get Started &rsaquo;</a>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>
      <!--end of section-->
    <?php include 'templates/footer-in.php'; ?>

    <?php require 'templates/scripts.php'; ?>


</body>

</html>