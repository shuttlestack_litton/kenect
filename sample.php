<?php 
require 'backend/db_config.php';
session_start();
 
$query = "SELECT * FROM users";
 
if ($result = $mysqli->query($query)) {
 
    while ($row = $result->fetch_assoc()) {
        $email = $row["email"];
        $first_name = $row['first_name'];
        $last_name = $row['last_name'];
        $username = $row['username'];
        $avatar = $row['avatar'];
    }
 
/*freeresultset*/
$result->free();
}
?>
<!doctype html>
<html lang="en" class="">
<?php $title = 'Home'; require 'templates/source.php'; ?>

<body>

    <?php include 'templates/header-in.php'; ?>

    <section class="flush-with-above">
        <div class="container">
          <div class="row">
            <div class="col">
              <div class="d-flex justify-content-between align-items-center mb-4">
                <div>
                  <span class="text-muted text-small">Requests 1 of 1</span>
                </div>
                <form class="d-flex align-items-center">
                  <span class="mr-2 text-muted text-small text-nowrap">Sort by:</span>
                  <select class="custom-select">
                    <option value="old-new" selected>Newest</option>
                    <option value="new-old">Oldest</option>
                  </select>
                </form>
              </div>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
          <div class="row">
            <div class="col">
              <table class="table table-borderless table-hover align-items-center">
                <thead>
                  <tr>
                    <th scope="col">Name</th>
                    <th scope="col">Genre</th>
                    <th scope="col">Album</th>
                    <th scope="col">Rating</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>

                  <tr class="bg-white">
                    <th scope="row">
                      <a class="media align-items-center" style="cursor: default;">
                        <img alt="Image" src="https://is1-ssl.mzstatic.com/image/thumb/Music128/v4/60/12/51/60125165-a4a3-231b-328d-a7d1a9e723f6/00602577257148.rgb.jpg/600x600bf.png" class="avatar rounded avatar-sm" />
                        <div class="media-body">
                          <span class="h6 mb-0">Lucid Dreams</span>
                          <span class="text-muted">Juice WRLD</span>
                        </div>
                      </a>
                    </th>
                    <td>Hip-Hop</td>
                    <td>Goodbye & Good Riddance</td>
                    <td>
                      <span class="badge badge-success">Good</span>
                    </td>
                    <td>
                     
                    </td>
                  </tr>
                  <tr class="table-divider">
                    <th></th>
                    <td></td>
                  </tr>

              
                  <tr class="table-divider">
                    <th></th>
                    <td></td>
                  </tr>

                </tbody>
              </table>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>
      
              <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal modal-center-viewport" role="document">
                      <div class="modal-content">
                        <div class="modal-header modal-header-borderless justify-content-end">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-times"></i>
                            </span>
                          </button>
                        </div>
                        <div class="modal-body d-flex justify-content-center">
                         
                        </div>
                        <div class="modal-footer modal-footer-borderless justify-content-center">
                           
                        </div>
                      </div>
                    </div>
                  </div>
           <section class="space-xs text-center bg-gradient text-light">
        <div class="container">
          <div class="row">
            <div class="col">
              <i class="mr-1 icon-cake"></i>
              <span class="mr-2">Please answer a few questions in our survey.</span>
              <a href="survey" class="text-white">Get Started &rsaquo;</a>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>
      <!--end of section-->
    <?php include 'templates/footer-in.php'; ?>

    <?php require 'templates/scripts.php'; ?>


</body>

</html>