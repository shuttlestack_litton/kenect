<?php 
require 'backend/db_config.php';
session_start();
 
$query = "SELECT * FROM users";
 
if ($result = $mysqli->query($query)) {
 
    while ($row = $result->fetch_assoc()) {
        $email = $row["email"];
        $first_name = $row['first_name'];
        $last_name = $row['last_name'];
        $username = $row['username'];
        $avatar = $row['avatar'];
    }

$result->free();
}
?>
<!doctype html>
<html lang="en" class="">
    <?php $title = 'Settings'; require 'templates/source.php'; ?>
    <body>

<?php include 'templates/header-in.php'; ?>
                  
 <section class="bg-white space-sm pb-4">
        <div class="container">
          <div class="row justify-content-between align-items-center">
            <div class="col-auto">
              <h1 class="h2">Account Settings</h1>
            </div>
            <!--end of col-->
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </section>
      <!--end of section-->
      <section class="flush-with-above space-0">
        <div class="bg-white">
          <div class="container">
            <div class="row">
              <div class="col">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="overview-tab" data-toggle="tab" href="#general" role="tab" aria-selected="true">Account</a>
                  </li>
                </ul>
              </div>
              <!--end of col-->
            </div>
            <!--end of row-->
          </div>
          <!--end of container-->
        </div>
      </section>
      <section class="flush-with-above height-80 d-block">
        <div class="tab-content">
          <div class="tab-pane fade show active" id="general" role="tabpanel">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="media flex-wrap mb-0 align-items-center">
                    <img alt="Image" src="<?php print $avatar; ?>" class="avatar avatar-lg mb-3 mb-md-0" />
                    <div class="media-body">

                    </div>
                  </div>
                </div>
                <!--end of col-->
              </div>
              <!--end of row-->
              <hr>
              <div class="row mb-4">
                <div class="col">
                  <h5>Account Details</h5>
                </div>
                <!--end of col-->
              </div>
              <!--end of row-->
              <div class="row">
                <!--end of col-->
                <div class="col-12 col-md-8 order-md-1">
                  <form class="row" action="" method="POST">
                    <div class="col-6">
                      <div class="form-group">
                        <label for="first-name">First Name:
                          <span class="text-red">*</span>
                        </label>
                        <input class="form-control form-control-lg" type="text" name="first_name" value="<?php print $first_name; ?>" id="first-name" disabled/>
                      </div>
                    </div>
                    <div class="col-6">
                      <div class="form-group">
                        <label for="last-name">Last Name:</label>
                        <span class="text-red">*</span>
                        <input class="form-control form-control-lg" type="text" name="last_name" value="<?php print $last_name; ?>" id="last-name" disabled/>
                      </div>
                    </div>
                     <div class="col-12">
                      <div class="form-group">
                        <label for="emailaddress">Username:
                          <span class="text-red">*</span>
                        </label>
                        <input class="form-control form-control-lg" type="email" value="<?php print $username; ?>" name="emailAddress" id="emailaddress" disabled />
                      </div>
                    </div>
                    <div class="col-12">
                      <div class="form-group">
                        <label for="emailaddress">Email Address:
                          <span class="text-red">*</span>
                        </label>
                        <input class="form-control form-control-lg" type="email" value="<?php print $email; ?>" name="emailAddress" id="emailaddress" disabled />
                        <small>Used to log in to your account</small>
                      </div>
                    </div>
                    <div class="col-12">
                      <input type="hidden" name="id" value=<?php print $_GET['id'];?>>
                      <div class="form-group">
                        <button class="btn btn-primary" name="update" type="submit">Save changes</button>
                      </div>
                    </div>
                  </form>
                </div>
                <!--end of col-->
              </div>
              <!--end of row-->
              <hr>
              <div class="row mb-4">
             
                <!--end of col-->
              </div>
              <!--end of row-->
            </div>
            <!--end of container-->
          </div>
          <!--end of tab pane-->
          </section>
      <!--end of section-->
      
      <?php include 'templates/footer-in.php'; ?>
                  
                  <?php require 'templates/scripts.php'; ?>

      </body>
      </html>