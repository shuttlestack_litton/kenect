<?php 
require 'backend/db_config.php';
session_start();
 
$query = "SELECT * FROM users";
 
if ($result = $mysqli->query($query)) {
 
    while ($row = $result->fetch_assoc()) {
        $email = $row["email"];
        $first_name = $row['first_name'];
        $last_name = $row['last_name'];
        $username = $row['username'];
        $avatar = $row['avatar'];
    }
 
/*freeresultset*/
$result->free();
}
?>
<!doctype html>
<html lang="en" class="">
<?php $title = 'Survey'; require 'templates/source.php'; ?>

<body>

    <?php include 'templates/header-in.php'; ?>

    <div class="typeform-widget" data-url="https://kenect.typeform.com/to/AnLcaG" style="width: 100%; height: 600px;"></div> <script> (function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })() </script> <div style="font-family: Sans-Serif;font-size: 12px;color: #999;opacity: 0.5; padding-top: 5px;"> powered by <a href="https://admin.typeform.com/signup?utm_campaign=AnLcaG&utm_source=typeform.com-13399345-Basic&utm_medium=typeform&utm_content=typeform-embedded-poweredbytypeform&utm_term=EN" style="color: #999" target="_blank">Typeform</a> </div>
    <?php include 'templates/footer-in.php'; ?>

    <?php require 'templates/scripts.php'; ?>


</body>

</html>