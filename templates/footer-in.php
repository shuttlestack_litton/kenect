<footer class="footer-short">
        <div class="container">
          <hr>
          <nav class="row justify-content-between align-items-center">
            <div class="col-auto">
              <ul class="list-inline">

                <li class="list-inline-item">
                  <a href="home">Home</a>
                </li>
                <li class="list-inline-item">
                  <a href="queue">Queue</a>
                </li>
                <li class="list-inline-item">
                  <a href="collection">Collection</a>
                </li>
                <li class="list-inline-item">
                  <a href="settings">Account Settings</a>
                </li>
              </ul>
            </div>
            <!--end of col-->
            
          </nav>
          <!--end of row-->
          <div class="row">
            <div class="col">
              <small>&copy; 2019 Kenect Inc. All Rights Reserved</small>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </footer>