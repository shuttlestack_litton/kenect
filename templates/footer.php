<footer class="footer-short">
        <div class="container">
          <hr>
          <nav class="row justify-content-between align-items-center">
            <div class="col-auto">
              <ul class="list-inline">

                <li class="list-inline-item">
                  <a href="/">Home</a>
                </li>
                <li class="list-inline-item">
                  <a href="/login" data-toggle="modal" data-target="#loginModal">Login</a>
                </li>
                <li class="list-inline-item">
                  <a onclick="createAccountFunction()" style="cursor: pointer;">Create An Account</a>
                </li>
              </ul>
            </div>
            <!--end of col-->
            <div class="col-auto text-sm-right">
              <ul class="list-inline">
                <li class="list-inline-item">
                  <a href="#"><i class="socicon-twitter"></i></a>
                </li>
                <li class="list-inline-item">
                  <a href="#"><i class="socicon-facebook"></i></a>
                </li>
              </ul>
            </div>
            <!--end of col-->
          </nav>
          <!--end of row-->
          <div class="row">
            <div class="col">
              <small>&copy; 2019 Kenect Inc. All Rights Reserved</small>
            </div>
            <!--end of col-->
          </div>
          <!--end of row-->
        </div>
        <!--end of container-->
      </footer>