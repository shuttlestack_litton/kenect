 <div class="navbar-container">
      <div class="navbar-dark" data-sticky="top" style="background: #1d1d1d;">
        <div class="container">
          <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand" href="home">
              <img alt="" src="static/img/logo.png" height="40" />
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fa fa-bars h4"></i>
            </button>
            <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a href="home" class="nav-link" style="font-weight: 500;">Home</a>
                </li>
                <li class="nav-item">
                  <a href="queue" class="nav-link" style="font-weight: 500;"> Queue</a>
                </li>
                <li class="nav-item">
                  <a href="collection" class="nav-link" style="font-weight: 500;"> Collection</a>
                </li>
              </ul>


              <ul class="navbar-nav">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle dropdown-toggle-no-arrow p-lg-0" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img alt="Image" src="<?php echo $avatar; ?>" class="avatar avatar-xs" style="border: solid 3px #0061d5;" />
                  </a>
                  <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="settings">Settings</a>
                    <a class="dropdown-item" href="profile">Profile</a>
                    <a class="dropdown-item" href="backend/logout">Log out</a>
                  </div>
                </li>
              </ul>

            </div>
            <!--end nav collapse-->
          </nav>
        </div>
        <!--end of container-->
      </div>
    </div>