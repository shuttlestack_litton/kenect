<div class="navbar-container">
    <div class="bg-dark navbar-dark" data-sticky="top">
        <div class="container">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand" href="/">
              <img alt="#" src="static/img/logo.png" height="40" />
            </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fas fa-bars" style="color: #fff;"></i>
            </button>
                <div class="collapse navbar-collapse justify-content-between" id="navbarNav">
                    <ul class="navbar-nav">
                    </ul>

                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a href="login" class="btn btn-success" data-toggle="modal" data-target="#loginModal" style="color: #000; ">Log In</a>
                        </li>
                    </ul>

                </div>
                <!--end nav collapse-->
            </nav>
        </div>
        <!--end of container-->
    </div>
</div>
