<head>
    <meta charset="utf-8">
    <title>Kenect | <?php echo $title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Kenect gives partygoers the ultimate nightlife experience, providing direct connection to the DJ, without barriers.">
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,400i,500" rel="stylesheet">
    <link href="static/css/theme.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
    <link rel="stylesheet" type="text/css" href="static/css/loading.css" />
    <link rel="stylesheet" type="text/css" href="static/css/loading-btn.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
    <script>
        window.addEventListener("load", function() {
            window.cookieconsent.initialise({
                "palette": {
                    "popup": {
                        "background": "#1d1d1d"
                    },
                    "button": {
                        "background": "#ffffff",
                        "text": "#1d1d1d"
                    }
                },
                "theme": "classic",
                "content": {
                    "message": "Kenect uses cookies to ensure you get the best experience on our platform.",
                    "dismiss": "I Accept",
                    "link": "Privacy Policy"
                }
            })
        });
    </script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>